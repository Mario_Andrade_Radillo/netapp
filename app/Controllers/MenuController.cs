using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace app.Controllers
{
    public class MenuController : Controller
    {
        public ActionResult Index()
        {
          Console.WriteLine("Index Method del MenuController");
          return View();
        }

    public ActionResult Angular()
    {
      Console.WriteLine("Angular Method del MenuController");
      return View("AngularIndex");
    }

    public ActionResult React()
    {
      Console.WriteLine("React Method del MenuController");
      return View("ReactIndex");
    }

  }
}
